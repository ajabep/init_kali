Init Kali
=========

To initialize a Kali Linux instance without pain.


Run it
======

Run this line:

```bash
bash -c ':(){ u=https://gitlab.com/ajabep/init_kali/raw/master/bootstrap.sh;which curl &>/dev/null;if test "$?" = "0";then curl $u|bash -s $u $*;else which wget &>/dev/null;if test "$?" = "0";then wget $u -O -|bash -s $u $*;else echo Oops, please install curl or wget;exit;fi;fi;}; : <ARGS>'
```


Fork it
=======

When you fork this repo, update the big shell line, above, and replace the URL,
at start of the function `:`, with the corresponding URL of your new repo.
