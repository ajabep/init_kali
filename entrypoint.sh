#!/bin/bash
# This script will initialize a Kali Linux
set -ex

ERROR() {
	printf '\033[1;31m%s\033[0m\n' "$*"
}

SUCCESS() {
	printf '\033[1;32m%s\033[0m\n' "$*"
}

WARNING() {
	printf '\033[1;33m%s\033[0m\n' "$*"
}

INFO() {
	printf '\033[1;34m%s\033[0m\n' "$*"
}

run_bg() {
	INFO "$2"
	/bin/bash "$1" "$debug" &
}

if [[ $# -lt 1 || $2 == '-h' || $2 == '--help' || $2 == '-help' ]]
then
	echo "Usage : $0 <URL where is hosted this script> <Args>"
	echo
	echo 'Args:'
	echo '	-h	--help	-help		Show this help message and exit'
	echo '	-d	--debug				Enable debug mode'
	exit 1
fi

# Parse args
_URL=$1 ; shift
debug=0
# shellcheck disable=SC2048
for arg in $@
do
	case $arg in
		'-d'|'--debug')
			debug=1
			;;
		*)
			ERROR "Not Yet Implemented"
			exit 1
	esac
done


INFO "Update APT"
while lsof /var/lib/dpkg/lock &>/dev/null
do
	sleep 5
done
systemctl stop packagekit.service || true
sleep 5
apt update
apt install -y wget

# Fix #4
touch /var/lock/aptitude

# Run scripts
cd "$(dirname "$0")/scripts"

run_bg ./misc_at_start.sh 'Execute misc actions which must be done'
run_bg ./apt_misc.sh 'Install misc things with APT'
run_bg ./dist-upgrade.sh 'Upgrade the OS'
run_bg ./git.sh 'Install Git'
run_bg ./gpg.sh 'Install GPG'
run_bg ./grub.sh 'Configure GRUB'
run_bg ./utils.sh 'Install utils for binary and network'
run_bg ./radare2.sh 'Install radare2'
run_bg ./ranger.sh 'Install Ranger'
run_bg ./rofi.sh 'Install Rofi'
run_bg ./tmux.sh 'Install Tmux'
run_bg ./proxychains.sh 'Install Proxychains'
run_bg ./socat.sh 'Install SoCat'
run_bg ./python.sh 'Install Python3'
run_bg ./htop.sh 'Install Htop'
run_bg ./httpie.sh 'Install HTTPie'
run_bg ./keepassx.sh 'Install KeePassX'
run_bg ./locate.sh 'Install locate and update db'
run_bg ./kali-specific.sh 'Install some tools and metapackage specific for Kali Linux'
run_bg ./virtualguesttools.sh 'Install Virtual Guest Tools'
run_bg ./vscode.sh 'Install VSCode'


# GPG
run_bg ./vagrant.sh 'Install Vagrant'

# Git
run_bg ./omz.sh 'Install OhMyZSH'
run_bg ./neovim.sh 'Install NeoVim'
run_bg ./gef.sh 'Install GEF for GDB'
run_bg ./findsploit.sh 'Install FindSploit'
run_bg ./linux-exploit-suggester.sh 'Install Linux Exploit Suggester'
run_bg ./unix-privesc-check.sh 'Install UNIX PrivEsc Check'
run_bg ./checksec.sh 'Install CheckSec'

# Needs apt-transport-https
run_bg ./docker.sh 'Install Docker'
run_bg ./metasploit.sh 'Install Metasploit'


#################

while true
do
	nb_jobs="$(jobs -l | grep -v "Done" | wc -l)"
	if [[ "$nb_jobs" == 0 ]]
	then
		break
	fi
	if [[ $debug == 1 ]]
	then
		jobs
		jobs -p
	fi
	sleep 5
done

SUCCESS "DONE :)"


if [[ $debug == 1 ]]
then
	SUCCESS "End of this script. Tap enter to quit"
	read -r
fi
