#!/bin/bash
# This script is the bootstrap. It will download this repo and execute it.
set -e

ERROR() {
	printf '\033[1;31m%s\033[0m\n' "$*"
}

SUCCESS() {
	printf '\033[1;32m%s\033[0m\n' "$*"
}

WARNING() {
	printf '\033[1;33m%s\033[0m\n' "$*"
}

INFO() {
	printf '\033[1;34m%s\033[0m\n' "$*"
}


run() {
	INFO "launch the entrypoint"
	# Not used $* because it make reference to args of this function, not to
	# those of the script
	# shellcheck disable=SC2086
	exec bash /tmp/installer/*/entrypoint.sh "$SCRIPT_URL" $ARGS
}

if [[ $# -lt 1 ]]
then
	echo "Usage : $0 <URL where is hosted this script> <Args>"
	echo
	# shellcheck disable=SC2016
	echo 'Args are passed to the next script. Give `-h` or `--help` to get help'
	exit 1
fi

SCRIPT_URL=$1 ; shift
ARGS=$*

if ls -la /tmp/installer/*/entrypoint.sh &>/dev/null
then
	INFO "The scripts are already downloaded, so skip download"
	run
	exit
fi


# Determine which program is installed
if which curl &>/dev/null
then
	dl() {
		url="$1"
		dst="$2"
		curl "$url" -o "$dst"
	}
else
	if which wget &>/dev/null
	then
		dl() {
			url="$1"
			dst="$2"
			wget "$url" -O "$dst"
		}
	else
		ERROR "Not Yet Implemented"
		exit 2
	fi
fi


# DL the repo
if (echo "$SCRIPT_URL" | grep -qi 'github')
then
	INFO "Detect that this script is hosted on Github"
	url_repo_targz="$(echo "$SCRIPT_URL" | sed 's/raw.githubusercontent.com/github.com/;s/\/\([^\/]*\)\/[^\/]*$/\/archive\/\1.tar.gz/')"
else
	INFO "Concider that this script is hosted on a Gitlab instance"
	# shellcheck disable=SC2001
	url_repo_targz="$(echo "$SCRIPT_URL" | sed 's#\([^/]*\)/raw/\([^/]*\)/.*$#\1/-/archive/\2/\1-\2.tar.gz#')"
fi

INFO "Download the repo"
dl "$url_repo_targz" /tmp/installer.tar.gz

INFO "Un-tar it"
mkdir /tmp/installer
cd /tmp/installer
tar xzf ../installer.tar.gz
cd -- *  # We don't know the name of the only directory inside the pwd

run
