#!/bin/bash
set -ex

SLEEP_APT=10
SLEEP_REQUIREMENTS=30

NOPE() {
	true
}

get_file_header() {
	headername="$1"
	grep -E "^# :$headername: " "$0" \
		| tail -n 1 \
		| sed "s/^# :$headername: //"
}

set_title() {
	title="$0"
	description=$(get_file_header "description")
	echo -ne "\033]0;$title\007"
	printf "\033[1;34m%s\n%s\033[0m\n" "$title" "$description"
}

debug=0
set_debug() {
	if [[ $# -gt 0 && "$1" == "1" ]]
	then
		debug=1
	fi
}

set_sigactions() {
	trap end EXIT
}


free_free_mutex_trap() {
	trap - ERR
	trap - INT
}
setup_free_mutex_trap() {
	trap free_apt_mutex ERR
	trap NOPE INT # Else, in case of Ctrl+C, the trap for ERR does not trigger
}
wait_and_lock_apt_mutex() {
	while true # Emulating mutex
	do
		if (trap - ERR ; set -C ; echo $$ >/tmp/installer/apt_lock)
		then
			setup_free_mutex_trap
			len_lockd_dpkg="$(lsof /var/lib/dpkg/lock;echo "$?")"
			len_lockd_aptitude="$(wc -l </var/lock/aptitude)"
			if [[ "$len_lockd_dpkg" == 1 && "$len_lockd_aptitude" == 0 ]]
			then
				break
			fi

			rm /tmp/installer/apt_lock
			free_free_mutex_trap
		fi

		sleep $SLEEP_APT
	done
}

free_apt_mutex() {
	rm /tmp/installer/apt_lock # Freed mutex
	free_free_mutex_trap
}

apt_install() {
	wait_and_lock_apt_mutex
	DEBIAN_FRONTEND=noninteractive apt install -y $@
	free_apt_mutex
}

wait_requirements() {
	requirements=$(get_file_header "require")
	for requirement in $requirements
	do
		while true
		do
			if grep -q "$requirement" /tmp/installer/provided
			then
				break
			else
				sleep $SLEEP_REQUIREMENTS
			fi
		done
	done
}

disable_service () {
	systemctl stop $@
	systemctl disable $@
}

end() {
	wait # wait unfinished jobs

	provides=$(get_file_header "provides")
	echo "$provides" >>/tmp/installer/provided

	if [[ $debug == 1 ]]
	then
		echo "End of this script. Tap enter to quit"
		read -r
	fi
}


set_title
set_debug $@
set_sigactions
