#!/bin/bash
# :description: Install proxychains
# :require:
# :use: apt
# :provides: proxychains proxychain

. ../script_utils.sh


apt_install proxychains proxychains4
