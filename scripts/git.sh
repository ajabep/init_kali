#!/bin/bash
# :description: Install and configure git
# :require:
# :use: apt wget
# :provides: git

# Customize these vars !
username=Ajabep
email=ajabep@tutanota.com


. ../script_utils.sh


cat - >~/.gitconfig <<EOF &
[user]
	name = $username
	email = $email
[core]
	excludesfile = ~/.gitignore
[pull]
	rebase = preserve
[init]
	defaultBranch = main
EOF

wget https://www.gitignore.io/api/archive,cvs,eclipse,gpg,jetbrains,libreoffice,linux,netbeans,notepadpp,svn,sublimetext,tags,vim,visualstudiocode,windows,vagrant -c -O ~/.gitignore &


apt_install git
