#!/bin/bash
# :description: Install Docker images to check quickly a k8s deployment
# :require: docker
# :use: docker
# :provides: kubesec aquasec

. ../script_utils.sh


wait_requirements

systemctl start docker
docker pull aquasec/trivy
docker pull kubesec/kubesec
docker pull aquasec/kube-bench
