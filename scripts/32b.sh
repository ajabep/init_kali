#!/bin/bash
# :description: Enable compilation and execution of 32b binaries
# :require:
# :use: apt
# :provides: 32b
# From https://www.cyberciti.biz/tips/compile-32bit-application-using-gcc-64-bit-linux.html
# Thank you Vivek Gite :D

. ../script_utils.sh


apt_install g++-multilib gcc-multilib libc6-dev-i386

