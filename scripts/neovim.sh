#!/bin/bash
# :description: Install NeoVim
# :require: git
# :use: apt
# :provides: nvim neovim vim vi

. ../script_utils.sh


wait_requirements


git clone --depth=1 https://github.com/amix/vimrc.git ~/.vim_runtime || (cd ~/.vim_runtime && git pull)
sh ~/.vim_runtime/install_awesome_vimrc.sh


apt_install neovim
