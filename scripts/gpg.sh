#!/bin/bash
# :description: Install and configure gpg
# :require:
# :use: apt
# :provides: gpg

. ../script_utils.sh


mkdir -p ~/.gnupg
cat - >~/.gnupg/gpg.conf <<EOF &
no-emit-version
no-comments
keyid-format 0xlong
with-fingerprint
list-options show-uid-validity
verify-options show-uid-validity
personal-compress-preferences Uncompressed ZIP BZIP2 ZLIB
personal-cipher-preferences AES256 AES192 AES
personal-digest-preferences SHA512 SHA384 SHA256 SHA224
cert-digest-algo SHA512
default-preference-list SHA512 SHA384 SHA256 SHA224 AES256 AES192 AES ZLIB BZIP2 ZIP Uncompressed
s2k-cipher-algo AES256
s2k-digest-algo SHA512
s2k-mode 3
s2k-count 65011712
EOF

apt_install gnupg
