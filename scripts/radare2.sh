#!/bin/bash
# :description: Install Radare2
# :require:
# :use: apt
# :provides: r2 rabin2 radiff2 radare2

. ../script_utils.sh


cat - <<EOF >~/.radare2.sh &
eco solarized
EOF


apt_install radare2


r2pm init
r2pm update
#r2pm install bpf
r2pm install r2msdn
apt_install libzip-dev
r2pm install winapi
#r2pm install r2dec
