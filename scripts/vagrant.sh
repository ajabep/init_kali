#!/bin/bash
# :description: Install vagrant
# :require: gpg
# :use: dpkg apt-get wget
# :provides: vagrant

. ../script_utils.sh


wait_requirements


gpg_key_vagrant=0x51852D87348FFC4C
last_vagrant_version=2.1.2

gpg --keyserver pool.sks-keyservers.net --recv-keys $gpg_key_vagrant

wget https://releases.hashicorp.com/vagrant/"$last_vagrant_version"/vagrant_"$last_vagrant_version"_x86_64.deb -c -O /tmp/vagrant.deb
wget https://releases.hashicorp.com/vagrant/"$last_vagrant_version"/vagrant_"$last_vagrant_version"_SHA256SUMS -c -O /tmp/vagrant.sha256
wget https://releases.hashicorp.com/vagrant/"$last_vagrant_version"/vagrant_"$last_vagrant_version"_SHA256SUMS.sig -c -O /tmp/vagrant.sha256.sig




if ! gpg --verify /tmp/vagrant.sha256.sig /tmp/vagrant.sha256
then
	echo "bad sig"
	exit 4
fi

cd /tmp


if ! grep '_x86_64.deb$' ./vagrant.sha256 | sed 's/[^ ]\+$/vagrant.deb/' | sha256sum -c
then
	echo "bad sum"
	exit 4
fi


wait_and_lock_apt_mutex
dpkg -i /tmp/vagrant.deb
DEBIAN_FRONTEND=noninteractive apt-get install -y --fix-missing
free_apt_mutex
