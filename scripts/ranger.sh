#!/bin/bash
# :description: Install ranger
# :require:
# :use: apt
# :provides: ranger

. ../script_utils.sh


mkdir -p ~/.config/ranger/
cat - >~/.config/ranger/rc.conf <<EOF &
map DD shell mv -t /home/${USER}/.Trash %s
map ds shell /usr/bin/shred -n 20 -f -z -x -u -v %s
map dS shell /usr/bin/find %s -type f -exec /usr/bin/shred -n 20 -f -z -x -u -v '{}' ';' ; /usr/bin/srm -d -r -z %s ; # use shred to shred qwickly and srm to shred folders
map <c-n>  eval fm.tab_new('%d')
map S shell zsh -c "cd %d; zsh"
map P paste
map <F2> console rename
map T console touch

set show_hidden true
set preview_images true
set preview_files true
EOF


apt_install ranger xsel
