#!/bin/bash
# :description: Install tmux
# :require:
# :use: apt
# :provides: tmux

. ../script_utils.sh


cat - >~/.tmux.conf <<EOF &
set -g default-terminal "tmux-256color"
set-window-option -g mode-keys vi
set -g history-limit 10000
set -g set-titles on
set -g set-titles-string "#T"
EOF

apt_install tmux
