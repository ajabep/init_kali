#!/bin/bash
# :description: Install some wordlists
# :require: 
# :use: dpkg apt-get wget
# :provides: wordlists seclists payloadsallthethings

. ../script_utils.sh


wait_requirements


wait_and_lock_apt_mutex
apt_install wordlists seclists payloadsallthethings
free_apt_mutex
