#!/bin/bash
# :description: Install locate and update db
# :require:
# :use: apt
# :provides: locate

. ../script_utils.sh

apt_install locate

updatedb
