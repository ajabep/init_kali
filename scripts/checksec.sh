#!/bin/bash
# :description: Install CheckSec
# :require: git
# :use:
# :provides: checksec CheckSec checkSec

. ../script_utils.sh


wait_requirements


git clone https://github.com/slimm609/checksec.sh /opt/checksec || (cd /opt/checksec && git pull)

ln -s /opt/checksec/extras/zsh/_checksec /usr/share/zsh/vendor-completions/
ln -s /opt/checksec/extras/man/checksec.7 /usr/share/man/man7/
