#!/bin/bash
# :description: Install some utils
# :require: pip
# :use: apt pip
# :provides: dig dnsutils ldnsutils readelf objdump binutils elfutils net-tools hexedit ltrace strace


. ../script_utils.sh
wait_requirements




cat - >>~/.zsh_profile <<EOF &
hashid() {
    nth --text "\$1"
    sth --text "\$1"
}
EOF


apt_install dnsutils ldnsutils binutils elfutils net-tools hexedit ltrace strace jq
gem install ssh_scan
pip3 install pwntools pywhat search-that-hash name-that-hash
