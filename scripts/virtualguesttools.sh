#!/bin/bash
# :description: Install Virtual Guest Tools
# :require:
# :use: apt
# :provides:

. ../script_utils.sh
wait_requirements


apt_install open-vm-tools-desktop fuse virtualbox-guest-x11