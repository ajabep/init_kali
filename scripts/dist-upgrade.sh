#!/bin/bash
# :description: Upgrade the OS
# :require:
# :use: apt
# :provides: upgrades

. ../script_utils.sh


wait_and_lock_apt_mutex
DEBIAN_FRONTEND=noninteractive apt-get dist-upgrade -o DPkg::Options::=--force-confnew -y
DEBIAN_FRONTEND=noninteractive apt-get autoremove --purge -y
DEBIAN_FRONTEND=noninteractive apt-get autoclean
free_apt_mutex
