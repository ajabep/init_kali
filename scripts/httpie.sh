#!/bin/bash
# :description: Install httpie
# :require:
# :use: apt
# :provides: httpie http

. ../script_utils.sh


apt_install httpie
