#!/bin/bash
# :description: Install some tools and metapackage specific for Kali Linux
# :require:
# :use: apt
# :provides: kali-tweaks kali-linux-core kali-linux-headless kali-tools-hardware kali-tools-gpu kali-tools-crypto-stego kali-tools-windows-resources kali-tools-information-gathering kali-tools-vulnerability kali-tools-web kali-tools-database kali-tools-passwords kali-tools-reverse-engineering kali-tools-exploitation kali-tools-sniffing-spoofing kali-tools-post-exploitation kali-tools-reporting kali-linux-large


. ../script_utils.sh

apt_install kali-tweaks \
    kali-linux-core \
    kali-linux-headless \
    kali-tools-hardware \
    kali-tools-gpu \
    kali-tools-crypto-stego \
    kali-tools-windows-resources \
    kali-tools-information-gathering \
    kali-tools-vulnerability \
    kali-tools-web \
    kali-tools-database \
    kali-tools-passwords \
    kali-tools-reverse-engineering \
    kali-tools-exploitation \
    kali-tools-sniffing-spoofing \
    kali-tools-post-exploitation \
    kali-tools-reporting \
    kali-linux-large
