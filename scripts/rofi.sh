#!/bin/bash
# :description: Install rofi
# :require:
# :use: apt
# :provides: rofi

. ../script_utils.sh


apt_install rofi
