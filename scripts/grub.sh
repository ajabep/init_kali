#!/bin/bash
# :description: remove wait time in grub
# :require:
# :use:
# :provides: grub-no-wait

. ../script_utils.sh


sed 's/GRUB_TIMEOUT=.*/GRUB_TIMEOUT=0/g' /etc/default/grub -i
update-grub
