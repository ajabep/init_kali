#!/bin/bash
# :description: Install py3
# :require:
# :use: apt
# :provides: py3 python3 pip3 python pip

. ../script_utils.sh


apt_install python3 python3-pip
