#!/bin/bash
# :description: Install KeePassX
# :require:
# :use: apt
# :provides: keepassx keepass

. ../script_utils.sh


apt_install keepassx
