#!/bin/bash
# :description: Install Docker and pull an alpine image
# :require: apt-transport-https
# :use: apt wget systemctl
# :provides: docker docker.service

. ../script_utils.sh


wait_requirements


wget https://download.docker.com/linux/debian/gpg -c -O - | gpg --dearmor - >/etc/apt/trusted.gpg.d/docker.gpg

cat - >/etc/apt/sources.list.d/docker.list <<EOF
deb [arch=amd64] https://download.docker.com/linux/debian stretch stable
EOF


wait_and_lock_apt_mutex
apt update
free_apt_mutex
apt_install docker-ce


systemctl start docker
docker pull alpine:3.7
