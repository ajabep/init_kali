#!/bin/bash
# :description: Install OhMyZsh
# :require: git
# :use: apt wget
# :provides: zsh omz ohmyzsh

. ../script_utils.sh


wait_requirements


apt_install zsh


echo | sh -c "$(wget https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh -O -)"

sed 's/ZSH_THEME[ =].*/ZSH_THEME="flazz"/g' -i ~/.zshrc &
cat - >>~/.zshrc <<EOF
source ~/.zsh_profile
EOF

cat - >>~/.zsh_profile <<EOF
export MSF_DATABASE_CONFIG=~/.msf4/database.yml
export PAGER=less
export EDITOR=vim
export BROWSER=firefox
export VAGRANT_DEFAULT_PROVIDER=virtualbox

alias :q='exit'
alias gti='git'
alias py='python3'
alias rnager='ranger'
alias egrep='egrep --color=auto'
alias shredFile='shred -n 20 -f -z -x -u -v'
alias wfuzz='wfuzz -c'
alias dd='dd status=progress'
alias ip='ip -c'
alias hashcat='hashcat --status'

venv() {
	if test "\$#" -ge "1"
	then
		venv=\$1
	else
		venv="venv"
	fi
	. "\$venv"/bin/activate
}

mkcd() {
	mkdir -p "\$1" && cd "\$1"
}


umask 077


# Hello
echo "▄▄▄▄▄ ▄▄▄▄  ▄   ▄"
echo "  █   █   █  █ █ "
echo "  █   █▀▀█    █  "
echo "  █   █  ▀▄  █   "
echo "▄   ▄  ▄▄▄  ▄▄▄▄  ▄▄▄▄  ▄▄▄▄▄ ▄▄▄▄ "
echo "█   █ █   █ █   █ █   █ █     █   █"
echo "█▀▀▀█ █▀▀▀█ █▀▀█  █   █ █▀▀▀▀ █▀▀█ "
echo "█   █ █   █ █  ▀▄ █▄▄▄▀ █▄▄▄▄ █  ▀▄"
EOF

chsh -s /usr/bin/zsh
