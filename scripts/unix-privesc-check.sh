#!/bin/bash
# :description: Install Unix PrivEsc Check
# :require:
# :use: apt
# :provides: unix-privesc-check upc upc.sh

. ../script_utils.sh


apt_install unix-privesc-check
