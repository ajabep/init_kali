#!/bin/bash
# :description: Install some misc tools/utils/transport about apt
# :require:
# :use: apt
# :provides: apt-transport-https aptitude

. ../script_utils.sh


apt_install aptitude apt-transport-https
