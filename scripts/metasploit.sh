#!/bin/bash
# :description: Install MSF
# :require: apt-transport-https omz
# :use: apt wget systemctl
# :provides: msfconsole msfvenom metasploit

. ../script_utils.sh


wait_requirements


wget https://apt.metasploit.com/metasploit-framework.gpg.key -c -O - | gpg --dearmor - >/etc/apt/trusted.gpg.d/metasploit-framework.gpg

cat - >/etc/apt/sources.list.d/metasploit-framework.list <<EOF
deb https://downloads.metasploit.com/data/releases/metasploit-framework/apt kali main
EOF


wait_and_lock_apt_mutex
apt update
free_apt_mutex
apt_install metasploit-framework

systemctl start postgresql
msfdb init || true  # Currently fails. Idk why.
echo - | msfconsole -q

cat - >>~/.msf4/msfconsole.rc <<EOF &
setg PROMPT %red%T%yel@%H %whi%undmsf%clr(%yel%J %grn%S%clr)
setg VERBOSE true

load alias
#load sqlmap
#load nessus

alias j "jobs -v"
alias s "sessions -v"
alias -f w "workspace"
alias handler "use exploit/multi/handler"
alias -f r "run"


setg exitonsession true
EOF


cat - >>~/.zsh_profile <<EOF &
alias msfconsole='msfconsole -q -a'
EOF
